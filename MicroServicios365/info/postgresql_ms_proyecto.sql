/*
 Navicat PostgreSQL Data Transfer

 Source Server         : lnvwinqa_13.2.1_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 130002
 Source Host           : lnvwinqa-cgtbl:5432
 Source Catalog        : ms365
 Source Schema         : ms_proyecto

 Target Server Type    : PostgreSQL
 Target Server Version : 130002
 File Encoding         : 65001

 Date: 09/09/2021 23:52:43
*/


-- ----------------------------
-- Sequence structure for proyectos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ms_proyecto"."proyectos_id_seq";
CREATE SEQUENCE "ms_proyecto"."proyectos_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for proyectos
-- ----------------------------
DROP TABLE IF EXISTS "ms_proyecto"."proyectos";
CREATE TABLE "ms_proyecto"."proyectos" (
  "id" int4 NOT NULL DEFAULT nextval('"ms_proyecto".proyectos_id_seq'::regclass),
  "created_at" timestamp(6),
  "descripcion" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "estado" bool NOT NULL,
  "fecha_expiracion" timestamp(6),
  "proyecto" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "updated_at" timestamp(6),
  "usuario_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of proyectos
-- ----------------------------
INSERT INTO "ms_proyecto"."proyectos" VALUES (1, '2020-08-15 00:00:00', 'proyecto 1', 'f', '2020-10-01 00:00:00', 'proyecto 1', NULL, 2);
INSERT INTO "ms_proyecto"."proyectos" VALUES (2, '2020-08-20 00:00:00', 'proyecto 2', 'f', '2020-12-20 00:00:00', 'proyecto 2', NULL, 3);
INSERT INTO "ms_proyecto"."proyectos" VALUES (3, '2021-08-20 00:00:00', 'proyecto 3', 't', '2021-11-01 00:00:00', 'proyecto 3', NULL, 3);
INSERT INTO "ms_proyecto"."proyectos" VALUES (4, '2021-08-29 13:59:56.256', 'test', 't', '2021-01-01 13:59:56.256', 'PROJECT 01', NULL, 2);
INSERT INTO "ms_proyecto"."proyectos" VALUES (5, '2021-08-29 14:10:34.464', 'test update', 'f', '2021-08-30 14:41:45.305', 'PROJECT X02', '2021-08-29 14:42:15.42', 1);
INSERT INTO "ms_proyecto"."proyectos" VALUES (7, '2021-09-09 23:01:50.395', 'test update', 'f', '2021-09-30 23:42:13.395', 'PROJECT X0299', '2021-09-09 23:43:36.286', 1);
INSERT INTO "ms_proyecto"."proyectos" VALUES (6, '2021-09-09 23:01:21.906', 'test update', 'f', '2021-09-30 23:40:28.243', 'PROJECT X02', '2021-09-09 23:43:56.227', 1);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ms_proyecto"."proyectos_id_seq"
OWNED BY "ms_proyecto"."proyectos"."id";
SELECT setval('"ms_proyecto"."proyectos_id_seq"', 8, true);

-- ----------------------------
-- Indexes structure for table proyectos
-- ----------------------------
CREATE INDEX "proyectos_usuario_id_idx" ON "ms_proyecto"."proyectos" USING btree (
  "usuario_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table proyectos
-- ----------------------------
ALTER TABLE "ms_proyecto"."proyectos" ADD CONSTRAINT "proyectos_pkey" PRIMARY KEY ("id");
