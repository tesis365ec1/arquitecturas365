/*
 Navicat PostgreSQL Data Transfer

 Source Server         : lnvwinqa_13.2.1_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 130002
 Source Host           : lnvwinqa-cgtbl:5432
 Source Catalog        : ms365
 Source Schema         : ms_proyecto_archivo

 Target Server Type    : PostgreSQL
 Target Server Version : 130002
 File Encoding         : 65001

 Date: 09/09/2021 23:53:00
*/


-- ----------------------------
-- Sequence structure for archivos_proyectos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ms_proyecto_archivo"."archivos_proyectos_id_seq";
CREATE SEQUENCE "ms_proyecto_archivo"."archivos_proyectos_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for archivos_proyectos
-- ----------------------------
DROP TABLE IF EXISTS "ms_proyecto_archivo"."archivos_proyectos";
CREATE TABLE "ms_proyecto_archivo"."archivos_proyectos" (
  "id" int4 NOT NULL DEFAULT nextval('"ms_proyecto_archivo".archivos_proyectos_id_seq'::regclass),
  "created_at" timestamp(6),
  "nombre" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "ruta" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "updated_at" timestamp(6),
  "proyecto_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of archivos_proyectos
-- ----------------------------
INSERT INTO "ms_proyecto_archivo"."archivos_proyectos" VALUES (5, '2021-08-30 22:49:02.655', 'TIT3', 'Test.pdf', NULL, 3);
INSERT INTO "ms_proyecto_archivo"."archivos_proyectos" VALUES (7, '2021-08-30 22:51:37.231', 'TIT5', 'Test.xlsx', NULL, 3);
INSERT INTO "ms_proyecto_archivo"."archivos_proyectos" VALUES (12, '2021-08-31 19:52:08.573', 'TITSOAP', 'Test.docx', NULL, 3);
INSERT INTO "ms_proyecto_archivo"."archivos_proyectos" VALUES (13, '2021-08-31 20:00:05.594', 'TITSOAP2', 'Test9999.docx', NULL, 3);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ms_proyecto_archivo"."archivos_proyectos_id_seq"
OWNED BY "ms_proyecto_archivo"."archivos_proyectos"."id";
SELECT setval('"ms_proyecto_archivo"."archivos_proyectos_id_seq"', 14, true);

-- ----------------------------
-- Indexes structure for table archivos_proyectos
-- ----------------------------
CREATE INDEX "archivos_proyectos_proyecto_id_idx" ON "ms_proyecto_archivo"."archivos_proyectos" USING btree (
  "proyecto_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table archivos_proyectos
-- ----------------------------
ALTER TABLE "ms_proyecto_archivo"."archivos_proyectos" ADD CONSTRAINT "uk_4s0wu8u6o0ufvsniv68k0r2sc" UNIQUE ("nombre");

-- ----------------------------
-- Primary Key structure for table archivos_proyectos
-- ----------------------------
ALTER TABLE "ms_proyecto_archivo"."archivos_proyectos" ADD CONSTRAINT "archivos_proyectos_pkey" PRIMARY KEY ("id");
